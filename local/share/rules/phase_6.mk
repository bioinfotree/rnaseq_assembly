# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# reference based genome assembly with trinity


# TODO:
#	perform check of bam
#	get statistics on bam
#	test if SamFormatConverter.jar is really faster then samtools view

FRAGMENT_UNPAIRED_1	?= 
FRAGMENT_UNPAIRED_2	?= 
FRAGMENT_UNPAIRED_3	?= 
	
FRAGMENT_PAIRED_LEFT_1	?= 
FRAGMENT_PAIRED_RIGHT_1	?= 

FRAGMENT_PAIRED_LEFT_2	?= 
FRAGMENT_PAIRED_RIGHT_2	?= 

REFERENCE ?=

MAX_INTRON_LENGTH ?=

logs:
	mkdir -p $@

fragment.unpaired.1.fq.gz:
	ln -sf $(FRAGMENT_UNPAIRED_1) $@
fragment.unpaired.2.fq.gz:
	ln -sf $(FRAGMENT_UNPAIRED_2) $@
fragment.unpaired.3.fq.gz:
	ln -sf $(FRAGMENT_UNPAIRED_3) $@
	
fragment.paired.left.1.fq.gz:
	ln -sf $(FRAGMENT_PAIRED_LEFT_1) $@
fragment.paired.right.1.fq.gz:
	ln -sf $(FRAGMENT_PAIRED_RIGHT_1) $@

fragment.paired.left.2.fq.gz:
	ln -sf $(FRAGMENT_PAIRED_LEFT_2) $@
fragment.paired.right.2.fq.gz:
	ln -sf $(FRAGMENT_PAIRED_RIGHT_2) $@

referece.gz:
	ln -sf $(REFERENCE) $@

referece.fasta: referece.gz
	zcat <$< >$@


%.fq: %.fq.gz
	zcat <$< >$@


fragment.paired.%.coordSorted.bam: logs referece.fasta fragment.paired.left.%.fq fragment.paired.right.%.fq
	!threads
	$(call load_modules); \
	alignReads.pl \
	--left $^3 \
	--right $^4 \
	--seqType fq \
	--target $^2 \
	--output $(basename $(basename $@)) \
	--aligner bowtie2 \
	-- \   * Any options after '--' are passed onward to the alignments programs *
	-p $$THREADNUM \
	2>&1 \
	| tee $</alignReads.pl.$@.log \
	&& ln -sf $(basename $(basename $@))/$@ $@


fragment.unpaired.%.coordSorted.bam: logs referece.fasta fragment.unpaired.%.fq
	!threads
	$(call load_modules); \
	alignReads.pl \
	--single $^3 \
	--seqType fq \	
	--target $^2 \
	--output $(basename $(basename $@)) \
	--aligner bowtie2 \
	-- \
	-p $$THREADNUM \
	2>&1 \
	| tee $</alignReads.pl.$@.log \
	&& ln -sf $(basename $(basename $@))/$@ $@


fragment.paired.coordSorted.unpaired.sam: logs fragment.paired.1.coordSorted.bam fragment.paired.2.coordSorted.bam fragment.unpaired.1.coordSorted.bam fragment.unpaired.2.coordSorted.bam
	$(call load_modules); \
	java_exec  \
	MergeSamFiles.jar \
	SORT_ORDER=coordinate \
	USE_THREADING=true \
	$(addprefix INPUT=,$(call rest,$^)) \
	OUTPUT=$@ \
	2>&1 \
	| tee $</MergeSamFiles.jar.$@.log

# # sort using picard tools
# SORTED_BAM = $(BAM:.bam=.sorted.bam)
# %.sorted.bam: %.bam
# 	java_exec SortSam.jar SORT_ORDER=coordinate INPUT=$< OUTPUT=$@
# 
# # generate indexes
# INDEXES = $(SORTED_BAM:.bam=.bai)
# %.sorted.bai: %.sorted.bam
# 	samtools index $< $@

# transform bam to sam
# SORTED_SAM = $(SORTED_BAM:.bam=.sam)
# %.sorted.sam: %.sorted.bam
# 	samtools view $< >$@
# 
# %.sorted.sam: %.sorted.bam
# 	java_exec SamFormatConverter.jar INPUT=$< OUTPUT=$@

Dir_fragment.paired.coordSorted.unpaired.sam.minC1.gff.listing: logs fragment.paired.coordSorted.unpaired.sam
	$(call load_modules); \
	prep_rnaseq_alignments_for_genome_assisted_assembly.pl \
	--coord_sorted_SAM $^2 \
	-I $(MAX_INTRON_LENGTH) \
	2>&1 \
	| tee $</prep_rnaseq_alignments_for_genome_assisted_assembly.pl.$@.log


read_files.lst: Dir_fragment.paired.coordSorted.unpaired.sam.minC1.gff.listing
	find $(basename $<) -name "*reads" >$@

trinity_GG.cmds: read_files.lst
	$(call load_modules); \
	GG_write_trinity_cmds.pl --reads_list_file $< --paired  >$@

ParaFly.flag: logs trinity_GG.cmds
	!threads
	$(call load_modules); \
	ParaFly -c $^2 -CPU $$THREADNUM -failed_cmds $(addsuffix .failed,$^2) -v \
	2>&1 \
	| tee $</ParaFly.$@.log \
	&& touch$@


Trinity_GG.fasta: ParaFly.flag
	$(call load_modules); \
	find Dir_*  -name "*inity.fasta" \
	| GG_trinity_accession_incrementer.pl >$@

.PHONY: test
test:
	@echo


ALL +=  logs \
	referece.gz \
	fragment.unpaired.1.fq.gz \
	fragment.unpaired.2.fq.gz \
	fragment.unpaired.3.fq.gz \
	fragment.paired.left.1.fq.gz \
	fragment.paired.right.1.fq.gz \
	fragment.paired.left.2.fq.gz \
	fragment.paired.right.2.fq.gz \
	fragment.paired.1.coordSorted.bam \
	fragment.paired.2.coordSorted.bam \
	fragment.unpaired.1.coordSorted.bam \
	fragment.unpaired.2.coordSorted.bam \
	fragment.paired.coordSorted.unpaired.sam \
	Dir_fragment.paired.coordSorted.unpaired.sam.minC1.gff.listing \
	read_files.lst \
	trinity_GG.cmds \
	ParaFly.flag \
	Trinity_GG.fasta



INTERMEDIATE +=

CLEAN += 

