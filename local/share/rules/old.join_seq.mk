# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:


1_SHORT_PAIRED_INTERLEAVED ?= ../../test2/Illumina_pub_pairends.fastq.gz
1_SHORT_SINGLE   ?=	      ../../test2/Illumina_pub_orphans.fastq.gz


1_short_paired_interleaved.gz:
	ln -sf $(1_SHORT_PAIRED_INTERLEAVED) $@
1_short_single.gz:
	ln -sf $(1_SHORT_SINGLE) $@




test.flash.extendedFrags.fastq.gz: 1_short_paired_interleaved.gz
	!threads
	flash --threads $$THREADNUM \
	--compress \
	--interleaved \
	--min-overlap=10 \
	--max-overlap=35 \
	--fragment-len=35 \
	--output-prefix=test.flash \
	$<

.PHONY: test
test:
	@echo






ALL +=	$(SEQ_GZ)
	


INTERMEDIATE += $(SEQ_FASTQ)

CLEAN +=
