# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:

# reference
REFERENCE ?=
# database
DB ?=
# EST from ncbi
EST ?=
# complete CDS
CDS_COMPLETE ?=
# partial CDS
CDS_PARTIAL ?=
# setting for Launch_PASA_pipeline.pl
MAX_INTRON_LENGTH ?=

extern ../phase_3/Trinity_DN.fasta as ALL_TRANSCRIPTS_DN
extern ../phase_4/Trinity_GG.fasta as ALL_TRANSCRIPTS_GG

# reference file
reference.fasta:
	ln -sf $(REFERENCE) $@

# transcripts from genome-guided assembly
all_transcripts_gg.fasta:
	ln -sf $(ALL_TRANSCRIPTS_GG) $@

# transcripts from denovo assembly
all_transcripts_dn.fasta:
	ln -sf $(ALL_TRANSCRIPTS_DN) $@

# cds_complete.fasta:
# 	ln -sf $(CDS_COMPLETE) $@
# 
# cds_partial.fasta:
# 	ln -sf $(CDS_PARTIAL) $@
# 
# est.fasta:
# 	ln -sf $(EST) $@

logs:
	mkdir -p $@


transcripts.fasta: all_transcripts_gg.fasta all_transcripts_dn.fasta
	cat $^ \
	| sed '/^$$/d' \
	| fasta2tab \
	| bsort -u --key=1,1 \
	| collapsesets --glue='\t' 2 \
	| cut -f 1,2 \
	| bsort \
	| tab2fasta 2 >$@
	

all_transcripts_dn.accs: all_transcripts_dn.fasta
	$(call load_modules); \
	cat $^ \
	| accession_extractor.pl >$@

# cds_complete.accs: cds_complete.fasta
# 	$(call load_modules); \
# 	cat $^ \
# 	| accession_extractor.pl >$@

# Install MySQL (www.mysql.com)
# and create a:
#	user/password with read-only access
#	user/password with all privileges

# "rename $PASAHOME/pasa_conf/pasa.CONFIG.template" to "conf.txt"
# edit curret fields:
# MYSQLSERVER=(your mysql server name)
# MYSQL_RO_USER=(mysql read-only username)
# MYSQL_RO_PASSWORD=(mysql read-only password)
# MYSQL_RW_USER=(mysql all privileges username)
# MYSQL_RW_PASSWORD=(mysql all privileges password)

# trick to generate configuration file
# 1. prepare multi-line variable
############## CONFIG FILE ##############
# create config file on the model of "$PASAHOME/pasa_conf/pasa.alignAssembly.Template.txt"
define CONFIG_FILE
## templated variables to be replaced exist as <__var_name__>

# MySQL settings
# N.B.: remember to create it
MYSQLDB=$(DB)


#######################################################
# Parameters to specify to specific scripts in pipeline
# create a key = "script_name" + ":" + "parameter" 
# assign a value as done above.

#script validate_alignments_in_db.dbi
validate_alignments_in_db.dbi:--MIN_PERCENT_ALIGNED=95
validate_alignments_in_db.dbi:--MIN_AVG_PER_ID=30

#script subcluster_builder.dbi
subcluster_builder.dbi:-m=50
endef


# 2. export the variable value as-is to the shell as an environment variable
export CONFIG_FILE

# 3. reference it from the shell as an environment variable
alignAssembly.config:
	@echo "$$CONFIG_FILE" >$@


# # if the database was already created (-C), use instead "-s int" where int is the pipeline step
# # from which you want restart. There are 30 steps.
# # Otherwise delete the database "pasa_db" created with the user "pasa_read-write" and pass "pasa"
# run_pasa: alignAssembly.config reference.fasta all_transcripts_gg.fasta logs
# 	!threads
# 	$(call load_modules); \
# 	Launch_PASA_pipeline.pl \
# 	-c $< \
# 	-C \
# 	-R \
# 	-g $^2 \
# 	-t $^3 \
# 	--ALIGNERS blat,gmap \
# 	--CPU $$THREADNUM \
# 	--MAX_INTRON_LENG $(MAX_INTRON_LENGTH) \
# 	2>&1 \
# 	| tee $^4/Launch_PASA_pipeline.pl.$@.log   * [tool].[taget].log * 



# commands for running PASA pipeline given in "Complete Example Using the Provided Sample Data"
# they use transcripts cleaned with seqclean and full length transcripts

# % ../scripts/Launch_PASA_pipeline.pl \
# -c alignAssembly.config \
# -C \
# -R \
# -g genome_sample.fasta \
# -t all_transcripts.fasta.clean \
# 	-T \   * flag,transcript db were trimmed using the TGI seqclean tool. *
# 	-u all_transcripts.fasta \   * value, transcript db containing untrimmed sequences (input to seqclean) *
# 	-f FL_accs.txt \   * file containing a list of fl-cdna accessions. *
# --ALIGNERS blat,gmap \
# --CPU 2
#  

$(DB).assemblies.fasta: alignAssembly.config reference.fasta transcripts.fasta all_transcripts_dn.accs logs
	!threads
	$(call load_modules); \
	Launch_PASA_pipeline.pl \
	-c $< \   * configuration file *
	-C \   * flag, create MYSQL database *
	-R \   * flag, run alignment/assembly pipeline *
	-g $^2 \   * genome sequence FASTA file (should contain annot db asmbl_id as header accession) *
	-t $^3 \   * transcript db *
	--TDN $^4 \   * file containing a list of accessions corresponding to Trinity (full) de novo assemblies (not genome-guided) *
	--ALIGNERS blat,gmap \   * aligners (available options include: gmap, blat... can run both using 'gmap,blat') *
	--CPU $$THREADNUM \
	--MAX_INTRON_LENG $(MAX_INTRON_LENGTH) \   * (max intron length parameter passed to GMAP or BLAT)  (default: 100000) *
	2>&1 \
	| tee $^5/Launch_PASA_pipeline.pl.$@.log   * [tool].[taget].log * 

 
compreh_init_build.fasta: logs $(DB).assemblies.fasta alignAssembly.config transcripts.fasta
	$(call load_modules); \
	build_comprehensive_transcriptome.dbi \
	-c $^3 \
	-t $^4 \
	--min_per_ID 95 \
	--min_per_aligned 30 \
	2>&1 \
	| tee $</build_comprehensive_transcriptome.dbi.$@.log; \   * [tool].[taget].log *
	ln -sf compreh_init_build/$@ .


.PHONY: test
test:
	@echo


ALL +=  reference.fasta \
	all_transcripts_gg.fasta \
	all_transcripts_dn.fasta \
	alignAssembly.config \
	$(DB).assemblies.fasta \
	compreh_init_build.fasta


INTERMEDIATE +=

CLEAN += 

