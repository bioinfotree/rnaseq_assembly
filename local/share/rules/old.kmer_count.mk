# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:
#	optimal kmer length (graphs?)
#	insert length: -ins_length
#	insert STDEV: -ins_length_sd
#	coverage cut-off: -cov_cutoff (default 3)
#	minimum lenght of fragments: -min_trans_lgth
#	produce amos file: -amos_file ? Convert amos to ace
#	when using multiple type of paired-end reds with multiple insert size give them in input from the shorter to the longer
#	optimize

LCOUNT 		?= 3
KMERS		?= 17

1_LONG_SINGLE    ?=
	
1_SHORT_PAIRED_1 ?=
1_SHORT_PAIRED_2 ?=
1_SHORT_SINGLE   ?=

2_SHORT_PAIRED_1 ?=
2_SHORT_PAIRED_2 ?=
2_SHORT_SINGLE   ?=


1_long_single.gz:
	ln -sf $(1_LONG_SINGLE) $@
	
1_short_paired_1.gz:
	ln -sf $(1_SHORT_PAIRED_1) $@
1_short_paired_2.gz:
	ln -sf $(1_SHORT_PAIRED_2) $@
1_short_single.gz:
	ln -sf $(1_SHORT_SINGLE) $@

2_short_paired_1.gz:
	ln -sf $(2_SHORT_PAIRED_1) $@
2_short_paired_2.gz:
	ln -sf $(2_SHORT_PAIRED_2) $@
2_short_single.gz:
	ln -sf $(2_SHORT_SINGLE) $@

SEQ_GZ		:= 1_short_paired_1.gz 1_short_paired_2.gz 1_short_single.gz

SEQ_FASTQ	= $(SEQ_GZ:.gz=.fastq)
%.fastq: %.gz
	zcat <$< >$@
	
##
# Seecer runs in several steps, output of each step could be saved
# so that Seecer can starts at the next step
#
##

# 1. Remove read IDs to save memory
# Output for each read files a temp file
SEQ_FASTQ_N = $(SEQ_GZ:.gz=.fastq.N)
%.fastq.N: %.fastq
	@echo
	@echo "++ Step 1: Replacing Ns ... and stripping off read IDs"
	@echo
	random_sub_N $^,$@


# make temporary directory
tmp_dir.mk:
	TMPDIR=$$(mktemp -d tmpXXXX || { echo "Failed to create temp dir"; exit 1;}); \
	printf "TMPDIR := %s" $$TMPDIR >$@;
	

include tmp_dir.mk


# run JELLYFISH to count kmers
jellyfish.stats: $(SEQ_FASTQ_N)
	!threads
	@echo
	@echo "++ Step 2: Running JELLYFISH to count kmers ..."
	@echo
	K=$(KMERS); \
	LCOUNT=$(LCOUNT); \
	jellyfish count \
	-m $$K \
	-o $(TMPDIR)/jf_tmp \
	-c 3 \
	-s 10000000 \
	-t $$THREADNUM \
	--stats=$@ \
	--both-strands \
	$^




# merge jellyfish  databases
$(TMPDIR)/jf_merged_$(KMERS): jellyfish.stats
	!threads
	@echo
	@echo "++ Step 3: Running JELLYFISH to merge count kmers ..."
	@echo
	N_TMP=$$(ls -1 $(TMPDIR)/jf_tmp_* | wc -l); \
	if [ $$N_TMP -eq 1 ]; then \
	mv $(TMPDIR)/jf_tmp_0 $(TMPDIR)/jf_merged_$(KMERS); \
	else \
	jellyfish merge $(TMPDIR)/jf_tmp_* -o $@; \
	$(RM) $(TMPDIR)/jf_tmp_*; \
	fi


# data for plot histogram
jellyfish.histo: $(TMPDIR)/jf_merged_$(KMERS)
	!threads
	@echo
	@echo "++ Step 4: Running JELLYFISH to create hostogram of k-mer occurence ..."
	@echo
	jellyfish histo \
	-o $@ \
	-t $$THREADNUM \
	$<

# plot histogram in R
jellyfish.histo.pdf: jellyfish.histo
	tr ' ' \\t <$< \
	| distrib_plot --type=histogram --breaks=$$(wc -l $< | cut -d ' ' -f1) --output=$@ 2



# more complete statistics
jellyfish.stats.verbose: $(TMPDIR)/jf_merged_$(KMERS)
	!threads
	@echo
	@echo "++ Step 5: Running JELLYFISH to create statistics of k-mer occurence ..."
	@echo
	jellyfish stats \
	-o $@ \
	$<

# dump lower counts. Needed for seecer
counts_$(KMERS)_$(LCOUNT): $(TMPDIR)/jf_merged_$(KMERS)
	@echo
	@echo "++ Step 6: Running JELLYFISH to dump k-mer occurence ..."
	@echo
	jellyfish dump --lower-count=$(LCOUNT) -c $< -o $@;


# count only 16-mers. It is very fast because count in memory.
# produce output in 3 columns: take 1 and 3 for produce the histogram
kmers_count.hist: $(SEQ_FASTQ)
	!threads
	kmers_count $(addprefix --input ,$^) --threads $$THREADNUM --max-occ 10000 --16mer-hist $@

# print histogram
kmers_count.hist.pdf: kmers_count.hist
	head -n -1 $< | distrib_plot --type=histogram --breaks=$$(wc -l $< | cut -d ' ' -f1) --output=$@ 3


.PHONY: test
test:
	@echo






ALL +=	$(SEQ_GZ) \
	tmp_dir.mk \
	$(TMPDIR)/jf_merged_$(KMERS) \
	counts_$(KMERS)_$(LCOUNT) \
	jellyfish.stats \
	jellyfish.stats.verbose \
	jellyfish.histo \
	jellyfish.histo.pdf


INTERMEDIATE += $(wildcard *.fastq) \
		$(SEQ_FASTQ) \
		$(SEQ_FASTQ_N)

CLEAN +=  $(TMPDIR)
