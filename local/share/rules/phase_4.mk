# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# statistics of the genome assembly and reads alignment

# TODO:

MIN_OCC ?= 0
MAX_OCC_IN_GRAPHS ?= 200
PRJ ?=

context prj/gap_analysis
extern ../phase_3/Trinity.fasta as TRINITY_FASTA
extern ../phase_3/jellyfish.kmers.fa.histo as JELLYFISH_HISTO


logs:
	mkdir -p $@

Trinity.fasta:
	ln -sf $(TRINITY_FASTA) $@

jellyfish.kmers.fa.histo:
	ln -sf $(JELLYFISH_HISTO) $@

# calculate graphs ########################

longest.isoform.fasta: Trinity.fasta
	$(call load_modules); \
	get_longest_isoform_seq_per_trinity_gene.pl $< >$@

%.length.pdf: %.fasta
	fasta_length $< \
	| this_distrib_plot -t histogram -b 240 --xlab="transcript length" --ylab="number of transcript" --title "$(PRJ) transcript lengths distribution" -o $@ 2



.META: jellyfish.kmers.fa.histo.count
	1	kmer_frequency
	2	num_distinct_kmers
	3	frac_distinct_kmers = (2)/num_distinct_kmers_total
	4	cummulative(2)
	5	cummulative(3) = (4)/num_distinct_kmers_total
	6	num_kmers = (1)*(2)
	7	frac_kmers = (6)/num_kmers_total
	8	cummulative(6)
	9	cummulative(7) = (8)/num_kmers_total

jellyfish.kmers.fa.histo.count: jellyfish.kmers.fa.histo
	TOTAL_NUM_DISTINCT_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$2; } END {print tot;}' <$<); \
	TOTAL_NUM_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$1*$$2; } END {print tot;}' <$<); \
	awk -v total_num_distinct_kmers=$$TOTAL_NUM_DISTINCT_KMERS \
	    -v total_num_kmers=$$TOTAL_NUM_KMERS \
	'BEGIN{ printf "# 1:kmer_frequency\t2:num_distinct_kmers\t3:2/total_num_distinct_kmers\t4:cummulative(2)\t5:4/total_num_distinct_kmers\t6:num_kmers (1)*(2)\t7:6/total_num_kmers\t8:cummulative(6)\t9:8/total_num_kmers\n"; \
		_4=0; \
	        _8=0; \
	        FS=" "; \
	        OFS="\t"; } \
	!/^[$$,\#+]/ \
	{ kmer_frequency = $$1; \
	  num_distinct_kmers = $$2; \
	  _3 = num_distinct_kmers / total_num_distinct_kmers; \
	  _4 += num_distinct_kmers; \
	  _5 = _4 / total_num_distinct_kmers; \
	  _6 = kmer_frequency * num_distinct_kmers; \
	  _7 = _6 / total_num_kmers; \
	  _8 += _6; \
	  _9 = _8 / total_num_kmers; \
	  print kmer_frequency, num_distinct_kmers, _3, _4, _5, _6, _7, _8, _9; \
	}' <$< >$@

# KmerSpectrumPlot.pl is part of ALLPATHS assembly package.
# It creates script for GNUplot and execute it by producing plots in .gif and .eps format
jellyfish.histo.gp: jellyfish.kmers.fa.histo.count
	$(call load_modules); \
	KmerSpectrumPlot.pl \
	SPECTRA="{$<}" \
	FREQ_MIN=$(MIN_OCC) \
	FREQ_MAX=$(MAX_OCC_IN_GRAPHS) \
	HEAD_OUT=$(basename $@) \
	GIF=1 \
	ORGANISM=$(PRJ)

# align reads on the transcripts ########################

coordSorted.bam: logs Trinity.fasta
	!threads
	$(call load_modules); \
	bowtie_PE_separate_then_join.pl \
	--seqType fq \
	--target $^2 \
	--output bowtie_out_dir \
	$(call bowtie_PE_separate_then_join_param,$$THREADNUM) \
	2>&1 \
	| tee $</bowtie_PE_separate_then_join.$@.log \   * [tool].[taget].log *
	&& ln -sf bowtie_out_dir/bowtie_out_dir.coordSorted.bam $@

nameSorted.bam: coordSorted.bam
	ln -sf bowtie_out_dir/bowtie_out_dir.nameSorted.bam $@

nameSorted.stats: nameSorted.bam
	$(call load_modules); \
	SAM_nameSorted_to_uniq_count_stats.pl $< \
	| sed -e '/^$$/d;s/^[ \t]*//;s/: /:\t/' >$@



.PHONY: test
test:
	@echo



ALL += logs \
	coordSorted.bam \
	nameSorted.bam \
	nameSorted.stats \
	longest.isoform.fasta \
	Trinity.length.pdf \
	jellyfish.histo.gp


INTERMEDIATE += 

CLEAN += bowtie_out_dir \
	$(wildcard Trinity.*) \
	$(wildcard jellyfish.*)