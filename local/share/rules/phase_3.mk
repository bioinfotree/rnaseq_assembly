# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# denovo genome assembly with trinity

# TODO:

logs:
	mkdir -p $@

# prepare input ########################

reads.left.fastq:
	$(call load_modules); \
	zcat $(call merge, ,$(FRAGMENT_PAIRED_LEFT) $(FRAGMENT_UNPAIRED)) \
	| fastq2tab \
	| bawk '!/^[$$,\#+]/ { split($$1,a," "); print a[1]"/1", $$2, $$3; }' \
	| tab2fastq >$@

reads.right.fastq:
	zcat $(call merge, ,$(FRAGMENT_PAIRED_RIGHT)) \
	| fastq2tab \
	| bawk '!/^[$$,\#+]/ { split($$1,a," "); print a[1]"/2", $$2, $$3; }' \
	| tab2fastq >$@

# run Trinity ########################

ifeq ($(RENAME_READS),True)

Trinity.fasta: logs reads.left.fastq reads.right.fastq
	!threads
	$(call load_modules); \
	Trinity \
	--seqType fq \   * type of reads *
	--CPU $$THREADNUM \
	--output trinity_out_dir \
	$(call trinity_param,$^2,$^3) \
	2>&1 \
	| tee $</Trinity.$@.log \   * [tool].[taget].log *
	&& ln -sf trinity_out_dir/$@

else

Trinity.fasta: logs
	!threads
	$(call load_modules); \
	Trinity \
	--seqType fq \   * type of reads *
	--CPU $$THREADNUM \
	--output trinity_out_dir \
	$(call trinity_param) \
	2>&1 \
	| tee $</Trinity.$@.log \   * [tool].[taget].log *
	&& ln -sf trinity_out_dir/$@

endif

# collect usefull files for statistics ########################

reads.left.fastq.readcount reads.right.fastq.readcount: Trinity.fasta
	touch $@

inchworm.K25.L25.DS.fa: Trinity.fasta
	if [ -s trinity_out_dir/$@ ]; then \
		cp trinity_out_dir/$@ $@; \
	fi

both.fa.read_count: Trinity.fasta
	if [ -s trinity_out_dir/$@ ]; then \
		cp trinity_out_dir/$@ $@; \
	fi

jellyfish.kmers.fa.histo: Trinity.fasta
	if [ -s trinity_out_dir/$@ ]; then \
		cp trinity_out_dir/$@ $@; \
	fi

bundled_iworm_contigs.fasta: Trinity.fasta
	if [ -s trinity_out_dir/chrysalis/$@ ]; then \
		cp trinity_out_dir/chrysalis/$@ $@; \
	fi

GraphFromIwormFasta.out: Trinity.fasta
	if [ -s trinity_out_dir/chrysalis/$@ ]; then \
		cp trinity_out_dir/chrysalis/$@ $@; \
	fi

readsToComponents.out.sort: Trinity.fasta
	if [ -s trinity_out_dir/chrysalis/$@ ]; then \
		cp trinity_out_dir/chrysalis/$@ $@; \
	fi

Trinity.timing: Trinity.fasta
	sed -E 's/^ *//' <trinity_out_dir/$@ \
	| sed -E 's/\s{2,}/\t/g' \
	| sed -e 's/s: /s:\t/;s/fasta /fasta\t/' >$@

# calculate statistics about transcripts ########################

Trinity.stats: Trinity.fasta reads.left.fastq.readcount reads.right.fastq.readcount
	$(call load_modules); \
	TrinityStats.pl $< \
	| sed -e '/^$$/d;s/^[ \t]*//;s/: /:\t/' \
	| bawk -v LEFT_COUNT="$$(cat $^2 | cut -d " " -f3)" -v RIGHT_COUNT="$$(cat $^3 | cut -d " " -f3)" '/Percent GC:/ \   * add counts for right and left reads *
	{ print; print "Left reads count:",LEFT_COUNT; print "Right reads count:",RIGHT_COUNT; next }1' >$@

# These rule seems to fail at random
# don't know why
.PRECIOUS: Trinity.input.stats
Trinity.input.stats:
	>$@; \
	echo -e "left\tcount\tsize" >>$@; \
	for F in $(call merge, ,$(FRAGMENT_PAIRED_LEFT) $(FRAGMENT_UNPAIRED)); do \
		SIZE="$$(stat --printf="%s" $$F)"; \
		COUNT="$$(zcat <$$F | fastq2tab | wc -l)"; \
		echo -e "$$(basename $$F)\t$$COUNT\t$$SIZE" >>$@; \
	done; \
	echo -e "right\tcount\tsize" >>$@; \
	for F in $(FRAGMENT_PAIRED_RIGHT); do \
		SIZE="$$(stat --printf="%s" $$F)"; \
		COUNT="$$(zcat <$$F | fastq2tab | wc -l)"; \
		echo -e "$$(basename $$F)\t$$COUNT\t$$SIZE" >>$@; \
	done



.PHONY: test
test:
	@echo $(call merge, ,$(FRAGMENT_PAIRED_LEFT) $(FRAGMENT_UNPAIRED))




ALL += logs \
	Trinity.fasta \
	Trinity.timing \
	Trinity.stats \
	inchworm.K25.L25.DS.fa \
	both.fa.read_count \
	jellyfish.kmers.fa.histo \
	bundled_iworm_contigs.fasta \
	GraphFromIwormFasta.out \
	readsToComponents.out.sort \
	Trinity.input.stats   * attention that this can fail *


INTERMEDIATE += reads.left.fastq \
	reads.right.fastq \
	reads.left.fastq.readcount \
	reads.right.fastq.readcount

CLEAN += trinity_out_dir
