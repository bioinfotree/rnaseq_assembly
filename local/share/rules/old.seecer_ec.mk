# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:
#	optimal kmer length (graphs?)
#	insert length: -ins_length
#	insert STDEV: -ins_length_sd
#	coverage cut-off: -cov_cutoff (default 3)
#	minimum lenght of fragments: -min_trans_lgth
#	produce amos file: -amos_file ? Convert amos to ace
#	when using multiple type of paired-end reds with multiple insert size give them in input from the shorter to the longer
#	optimize

LCOUNT 		?= 3
KMERS		?= 17

1_LONG_SINGLE    ?=
	
1_SHORT_PAIRED_1 ?=
1_SHORT_PAIRED_1 ?=
1_SHORT_SINGLE   ?=

2_SHORT_PAIRED_1 ?=
2_SHORT_PAIRED_1 ?=
2_SHORT_SINGLE   ?=


1_long_single.gz:
	ln -sf $(1_LONG_SINGLE) $@
	
1_short_paired_1.gz:
	ln -sf $(1_SHORT_PAIRED_1) $@
1_short_paired_2.gz:
	ln -sf $(1_SHORT_PAIRED_1) $@
1_short_single.gz:
	ln -sf $(1_SHORT_SINGLE) $@

2_short_paired_1.gz:
	ln -sf $(2_SHORT_PAIRED_1) $@
2_short_paired_2.gz:
	ln -sf $(2_SHORT_PAIRED_1) $@
2_short_single.gz:
	ln -sf $(2_SHORT_SINGLE) $@

SEQ_GZ		:= 1_short_paired_1.gz 1_short_paired_2.gz 1_short_single.gz

SEQ_FASTQ	= $(SEQ_GZ:.gz=.fastq)


%.fastq: %.gz
	zcat <$< >$@
	
##
# Seecer runs in several steps, output of each step could be saved
# so that Seecer can starts at the next step
#
##

# 1. Remove read IDs to save memory
# Output for each read files a temp file
SEQ_FASTQ_N = $(SEQ_GZ:.gz=.fastq.N)
%.fastq.N: %.fastq
	@echo
	@echo "++ Step 1: Replacing Ns ... and stripping off read IDs"
	@echo
	random_sub_N $^,$@


# make temporary directory
tmp_dir.mk:
	TMPDIR=$$(mktemp -d tmpXXXX || { echo "Failed to create temp dir"; exit 1;}); \
	printf "TMPDIR := %s" $$TMPDIR >$@;
	


include tmp_dir.mk


# run JELLYFISH to count kmers
jellyfish.stats: $(SEQ_FASTQ_N)
	!threads
	echo; \
	echo "++ Step 2: Running JELLYFISH to count kmers ..."; \
	echo; \
	K=$(KMERS); \
	LCOUNT=$(LCOUNT); \
	jellyfish count \
	-m $$K \
	-o $(TMPDIR)/jf_tmp \
	-c 3 \
	-s 10000000 \
	-t $$THREADNUM \
	--stats=$@ \
	--both-strands \
	$^




# merge jellyfish  databases
$(TMPDIR)/jf_merged_$(KMERS): jellyfish.stats
	!threads
	echo; \
	echo "++ Step 3: Running JELLYFISH to merge count kmers ..."; \
	echo; \
	N_TMP=$$(ls -1 $(TMPDIR)/jf_tmp_* | wc -l); \
	if [ $$N_TMP -eq 1 ]; then \
	mv $(TMPDIR)/jf_tmp_0 $(TMPDIR)/jf_merged_$(KMERS); \
	else \
	jellyfish merge $(TMPDIR)/jf_tmp_* -o $@; \
	$(RM) $(TMPDIR)/jf_tmp_*; \
	fi


# data for plot histogram
jellyfish.histo: $(TMPDIR)/jf_merged_$(KMERS)
	!threads
	echo; \
	echo "++ Step 4: Running JELLYFISH to create hostogram of k-mer occurence ..."; \
	echo; \
	jellyfish histo \
	-o $@ \
	-t $$THREADNUM \
	$<

# plot histogram in R
jellyfish.histo.pdf: jellyfish.histo
	tr ' ' \\t <$< \
	| distrib_plot --type=histogram --breaks=$$(wc -l $< | cut -d ' ' -f1) --output=$@ 2



# more complete statistics
jellyfish.stats.verbose: $(TMPDIR)/jf_merged_$(KMERS)
	!threads
	echo; \
	echo "++ Step 5: Running JELLYFISH to create statistics of k-mer occurence ..."; \
	echo; \
	jellyfish stats \
	-o $@ \
	$<

# dump lower counts. Needed for seecer
$(TMPDIR)/counts_$(KMERS)_$(LCOUNT): $(TMPDIR)/jf_merged_$(KMERS)
	echo; \
	echo "++ Step 6: Running JELLYFISH to dump k-mer occurence ..."; \
	echo; \
	jellyfish dump --lower-count=$(LCOUNT) -c $< -o $@;


SEQ_FASTQ_CRT_TMP = $(SEQ_FASTQ_N:.fastq.N=.fastq.crt.tmp)

# seecer main part: correcting errrors
# remember to use the fastq files with ambiguous bases Ns replaced
# some parameters of seecer are note mentioned on the manual nor in the help but are corectly parsed (es: -o --output)
%.fastq.crt.tmp: $(TMPDIR)/counts_$(KMERS)_$(LCOUNT) %.fastq.N
	echo; \
	echo "++ Step 7: Correcting errors with SEECER ... Your reads are in good hands!"; \
	echo; \
	echo " *** Start time: " `date`; \
	seecer --kmer $(KMERS) --kmerCount $< --output $@ $^2; \
	echo " *** End time: " `date`;


# put back the original read IDs
SEQ_FASTQ_CRT = $(SEQ_FASTQ_CRT_TMP:.fastq.crt.tmp=.fastq.crt)
%.fastq.crt: %.fastq.crt.tmp %.fastq %.fastq.N
	echo "++ Step 8: Cleaning and putting back original read IDs ... We finish soon!"; \
	replace_ids $< $^2 $^3 $@

# test pipelines
test.pipe: 1_short_paired_1.fastq 1_short_paired_2.fastq
	run_seecer -t $(TMPDIR) -s 4 $< $^2



.PHONY: test
test:
	@echo






ALL +=	$(SEQ_GZ) \
	$(SEQ_FASTQ) \
	$(SEQ_FASTQ_N) \
	$(TMPDIR)/jf_merged_$(KMERS) \
	$(TMPDIR)/counts_$(KMERS)_$(LCOUNT) \
	$(SEQ_FASTQ_CRT_TMP) \
	$(SEQ_FASTQ_CRT)



INTERMEDIATE += $(wildcard *.fastq)
#SEQ_FASTQ_CRT_TMP

CLEAN +=  
