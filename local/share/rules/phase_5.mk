# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# TODO:

extern ../phase_3/Trinity.fasta as TRINITY_FASTA


log:
	mkdir -p $@

reference.fasta:
	ln -sf $(TRINITY_FASTA) $@

reference.fasta.bwt: log reference.fasta
	$(call load_modules); \
	bwa index $^2 \
	2>&1 \
	| tee $</$(basename $@).log


results: log reference.fasta.bwt
	!threads
	$(call load_modules); \
	bwa mem -t $$THREADNUM \
	$(basename $^2) \
	<(zcat $(FRAGMENT_PAIRED_LEFT1)) <(zcat $(FRAGMENT_PAIRED_RIGHT1)) \
	2>$</bwa-mem.$@.log \
	| samtools view -bS - \
	| samtools sort -@ $$THREADNUM - $@

.PHONY: test
test:
	@echo



ALL += log \
	reference.fasta \
	reference.fasta.bwt \
	results


INTERMEDIATE += 

CLEAN += 