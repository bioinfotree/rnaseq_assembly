# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>


# TODO:
#	optimal kmer length (graphs?)
#	insert length: -ins_length
#	insert STDEV: -ins_length_sd
#	coverage cut-off: -cov_cutoff (default 3)
#	minimum lenght of fragments: -min_trans_lgth
#	produce amos file: -amos_file ? Convert amos to ace
#	when using multiple type of paired-end reds with multiple insert size give them in input from the shorter to the longer
#	optimize

LONG_SINGLE_1  ?=
SHORT_PAIRED_1 ?=
SHORT_SINGLE_1 ?=
SHORT_PAIRED_2 ?=
SHORT_SINGLE_2 ?=
MAX_KMER ?= 27


long_single_1:
	ln -sf $(LONG_SINGLE_1) $@
short_paired_1:
	ln -sf $(SHORT_PAIRED_1) $@
short_single_1:
	ln -sf $(SHORT_SINGLE_1) $@
short_paired_2:
	ln -sf $(SHORT_PAIRED_2) $@
short_single_2:
	ln -sf $(SHORT_SINGLE_2) $@


.PHONY: compile
compile:
	cd $$PRJ_ROOT/local/src; \
	if [ -s Makefile ]; then \
	  $(MAKE) -f Makefile distclean; \
	  $(MAKE) -f Makefile MAXKMERLENGTH=$$(echo "$(MAX_KMER) + 1" | bc) all; \
	fi


oasesAssemblyMix_$(MAX_KMER): long_single_1 short_paired_1 short_single_1 short_paired_2 short_single_2 compile
	!threads
	export OMP_THREAD_LIMIT=$$THREADNUM; \
	export OMP_NUM_THREADS=$$THREADNUM; \
	oases_pipeline_custom \
	-s 25-$(MAX_KMER):2 \   * kmer step *
	-o oasesAssemblyMix \   * output prefix *
	-d "-fastq.gz -long $< \   * velvet file descriptors *
	-fastq.gz -shortPaired $^2 \
	-fastq.gz -short $^3 \
	-fastq.gz -shortPaired $^4 \
	-fastq.gz -short $^5" \
	-p "-ins_length 500 -ins_length_sd 150 \   * insert size and STDEV of the firt paired-end dataset *
	-ins_length2 500 -ins_length2_sd 150 \   * insert size and STDEV of the second paired-end dataset *
	-unused_reads yes \   * obtain reads unused in the assembly *
	-min_trans_lgth 100 \   * minimum length of obtained transcripts *
	-amos_file yes \   * get amos file of the assembly *
	-cov_cutoff 5 \   * set minimum coverage to get a contig (def 3) *
	-min_pair_count 4" \   * number of pared reads needed to corroborate a connetion between 2 contigs (def 4) *
	2>singleKAssemblies.err



singleKAssemblies.err: oasesAssemblyMix_$(MAX_KMER)
	touch $@


.PHONY: compile4merge
compile4merge: oasesAssemblyMix_$(MAX_KMER)
	cd $$PRJ_ROOT/local/src; \
	if [ -s Makefile ]; then \
	  $(MAKE) -f Makefile MAXKMERLENGTH=$$(echo "$(MAX_KMER) + 1" | bc) LONGSEQUENCES=1 all; \
	fi

oasesAssemblyMixMerged: oasesAssemblyMix_$(MAX_KMER) compile4merge
	!threads
	export OMP_THREAD_LIMIT=$$THREADNUM; \
	export OMP_NUM_THREADS=$$THREADNUM; \
	oases_pipeline_custom \
	-s 25-$(MAX_KMER):2 \   * kmer step *
	-g 27 \   * kmer for marging *
	-o oasesAssemblyMix \   * output prefix *
	2>mergeAssemblies.err


mergeAssemblies.err: oasesAssemblyMixMerged
	touch $@




.PHONY: test
test:
	@echo






ALL += compile \
	  oasesAssemblyMix_$(MAX_KMER) \
	  singleKAssemblies.err \
	  compile4merge \
	  oasesAssemblyMixMerged \
	  mergeAssemblies.err


INTERMEDIATE +=

CLEAN +=  long_single_1 \
		short_paired_1 \
		short_single_1 \
		short_paired_2 \
		short_single_2 \
		singleKAssemblies.err \
		mergeAssemblies.err \
		$(wildcard oasesAssemblyMix*)
