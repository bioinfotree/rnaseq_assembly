# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# program versions
VERSION_VELVET := 1.2.10
VERSION_OASES  := 0.2.08

# categories of reads which handled independently
CATEGORIES     ?= 5
# max k-mer length
MAXKMERLENGTH  ?= 67
# set 1 if you have more than 2.2 billion reads
BIGASSEMBLY    ?=
# set 1 if you are assembling contigs longer than 32kb long
LONGSEQUENCES  ?=
# set 1 to turn on multithreading computation
OPENMP         ?= 1
# set 1 to use zlib source code distributed within the Velvet source package
BUNDLEDZLIB    ?=



BINDIST_VELVET := velvet_$(VERSION_VELVET).tgz
URL_VELVET     := http://www.ebi.ac.uk/~zerbino/velvet/$(BINDIST_VELVET)

BINDIST_OASES  := oases_$(VERSION_OASES).tgz
URL_OASES      := http://www.ebi.ac.uk/~zerbino/oases/$(BINDIST_OASES)


$(BINDIST_VELVET):
	wget -O"$@" '$(URL_VELVET)'

$(BINDIST_OASES):
	wget -O"$@" '$(URL_OASES)'


.PHONY: compile_velvet compile_oases install all distclean


compile_velvet: $(BINDIST_VELVET)
	mkdir -p $(basename $<); \
	tar -xvz -C $(basename $<) --strip-components 1 -f $<; \
	cd $(basename $<); \
	make 'CATEGORIES=$(CATEGORIES)' \
	'MAXKMERLENGTH=$(MAXKMERLENGTH)' \
	'BIGASSEMBLY=$(BIGASSEMBLY)' \
	'LONGSEQUENCES=$(LONGSEQUENCES)' \
	'OPENMP=$(OPENMP)' \
	'BUNDLEDZLIB=$(BUNDLEDZLIB)'; \
	cd ..

compile_oases: $(BINDIST_OASES) compile_velvet
	mkdir -p $(basename $<); \
	tar -xvz -C $(basename $<) --strip-components 1 -f $<; \
	cd $(basename $<); \
	make 'VELVET_DIR=../$(basename $(BINDIST_VELVET))' \
	'CATEGORIES=$(CATEGORIES)' \
	'MAXKMERLENGTH=$(MAXKMERLENGTH)' \
	'BIGASSEMBLY=$(BIGASSEMBLY)' \
	'LONGSEQUENCES=$(LONGSEQUENCES)' \
	'OPENMP=$(OPENMP)' \
	'BUNDLEDZLIB=$(BUNDLEDZLIB)'; \
	cd ..


define install_function
	set -e; \
	base='$1'; \
	bin='../bin'; \
	install -d "$$bin"; \
	for f in $$base/*; do \
	  if [ -f "$$f" -a -x "$$f" ]; then \
	    install --mode 770 "$$f" "$$bin"; \
	  fi; \
	done;
endef


install: compile_velvet compile_oases
	$(call install_function,$(basename $(BINDIST_VELVET)))
	$(call install_function,$(basename $(BINDIST_OASES)))


all:    install

distclean:
	rm -f $(BINDIST_VELVET) \
	rm -f $(BINDIST_OASES)
