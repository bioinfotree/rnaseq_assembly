#!/usr/bin/env python

import sys
import subprocess
import optparse
import shutil
import os
import re

##########################################
## Options and defaults
##########################################
def getOptions():
	parser = optparse.OptionParser('usage: %prog [options] --data "velveth file descriptors"')
	
	parser.add_option('-d', '--data',dest='data',help='Velveth file descriptors',metavar='FILES', default='')
	
	parser.add_option('-p', '--options',dest='oasesOptions',help='Oases options that are passed to the command line, e.g., -cov_cutoff 5 -ins_length 300 ',metavar='OPTIONS', default='')
	#parser.add_option('-m', '--kmin',dest='kmin',type="int",help='Minimum k',default=19)
	#parser.add_option('-M', '--kmax',dest='kmax',type="int",help='Maximum k',default=31)
	
	parser.add_option('-s', '--krange',dest='krange',help='Define range of k-mers to be used for computations in the form: kstart-kend:kstep. Default: 19-31:2',default="19-31:2")
# ,default=27

	parser.add_option('-a', '--assemble',dest='assemble',help='If defined perform the assemblies in the defined k-mers range',action='store_true',default=False)

	parser.add_option('-g', '--merge',dest='kmerge',type="int",help='If defined perform the merge action using k-mer passed as value. Recommend value: 27')
	
	parser.add_option('-o', '--output',dest='directoryRoot',help='Output directory prefix',metavar='NAME',default='oasesPipeline')
	
	parser.add_option('-c', '--clean',dest='clean',help='Clean temp files',action='store_true',default=False)
	
	parser.add_option('-u', '--debug',dest='debug',help='Debug mode: just print commands passed to Velvet and Oases andrecommend exits',action='store_true',default=False)
	options, args = parser.parse_args()

	if not options.kmerge and len(options.data) == 0:
		parser.print_help()
		print ''
		print 'You forgot to provide some data files!'
		print 'Current options are:'
		print options
		sys.exit(1)
	return options



# parse options.krange and return kstar,kstop,kstep if "start-end:kstep" is defined
def get_kmers(options):
	kmin = int(19)
	kmax = int(31)
	kstep = int(2)
	token = re.compile('[ |\-|:]').split(options.krange)
	if token[0]:
		kmin = int(token[0])
	if token[1]:
		kmax = int(token[1])
	if token[2]:
		kstep = int(token[-1])

	return kmin,kmax,kstep



##########################################
## Assembly procedure
##########################################
def singleKAssemblies(options,kmin,kmax,kstep):
	cmd = ['velveth', options.directoryRoot, '%i,%i,%i' % (kmin, kmax+kstep, kstep)] + options.data.split()
	if options.debug:
		print "singleKAssemblies commands:\n"
		print " ".join(cmd)
	else:
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
		output = p.communicate()
		assert p.returncode == 0, output[0] + "Hash failed\n"

	for k in range(kmin, kmax+kstep, kstep):
		cmd = ['velvetg','%s_%i' % (options.directoryRoot, k), '-read_trkg', 'yes']
		cmd2 = ['oases','%s_%i' % (options.directoryRoot, k)] + options.oasesOptions.split()
		if options.debug:
			print " ".join(cmd)
			print " ".join(cmd2)
		else:
			p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
			output = p.communicate()
			assert p.returncode == 0, "Velvetg failed at k = %i\n%s" % (k, output[0])

			p = subprocess.Popen(cmd2, stdout=subprocess.PIPE)
			output = p.communicate()
			assert p.returncode == 0, "Oases failed at k = %i\n%s" % (k, output[0])







def mergeAssemblies(options,kmin,kmax,kstep):
	files = ["%s_%i/transcripts.fa" % (options.directoryRoot, X) for X in range(kmin, kmax+kstep, kstep)]
	cmd = ['velveth','%sMerged' % options.directoryRoot, str(options.kmerge), '-long'] + files
	cmd2 = ['velvetg','%sMerged' % options.directoryRoot,'-conserveLong','yes','-read_trkg','yes']
	cmd3 = ['oases','%sMerged' % options.directoryRoot,'-merge','yes']

	if options.debug:
		print "\nmergeAssemblies commands:\n"
		print " ".join(cmd)
		print " ".join(cmd2)
		print " ".join(cmd3)
	else:
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
		output = p.communicate()
		assert p.returncode == 0, output[0] + "Velveth failed at merge\n"

		p = subprocess.Popen(cmd2, stdout=subprocess.PIPE)
		output = p.communicate()
		assert p.returncode == 0, output[0] + "Velvetg failed at merge\n"

		p = subprocess.Popen(cmd3, stdout=subprocess.PIPE)
		output = p.communicate()
		assert p.returncode == 0, output[0] + "Oases failed merge\n"

##########################################
## Checking dependencies
##########################################
def checkVelvet():
	try:
		p = subprocess.Popen(['velveth'], stdout=subprocess.PIPE)
	except OSError:
		print "Could not find Velvet"
		print "Make sure that it is properly installed on your path"
		sys.exit(1)
	for line in p.stdout:
		items = line.strip().split(' ')
		if items[0] == 'Version':
			items2 = map(int, items[1].split('.'))
			assert items2 >= [1,1,7], "Velvet must have version 1.1.07 or higher (currently %s)" % items[1]
			return
	assert False


def checkOases():
	try:
		p = subprocess.Popen(['oases'], stdout=subprocess.PIPE)
	except OSError:
		print "Could not find Oases"
		print "Make sure that it is properly installed on your path"
		sys.exit(1)
	for line in p.stdout:
		items = line.strip().split(' ')
		if items[0] == 'Version':
			items2 = map(int, items[1].split('.'))
			assert items2 >= [0,2,1], "Oases must have version 0.2.01 or higher (currently %s)" % items[1]
			return
	assert False

##########################################
## Clean up
##########################################
def clean(options,kmin,kmax,kstep):
	for k in range(kmin, kmax, kstep):
		shutil.rmtree("%s_%i" % (options.directoryRoot, k))
	os.remove("%sMerged/Sequences" % options.directoryRoot)
	os.remove("%sMerged/Roadmaps" % options.directoryRoot)
	os.remove("%sMerged/PreGraph" % options.directoryRoot)
	os.remove("%sMerged/Graph2" % options.directoryRoot)
	os.remove("%sMerged/LastGraph" % options.directoryRoot)
	os.remove("%sMerged/contigs.fa" % options.directoryRoot)
	os.remove("%sMerged/Log" % options.directoryRoot)

##########################################
## Master function
##########################################
def main():
	options = getOptions()
	checkVelvet()
	checkOases()
	kmin,kmax,kstep = get_kmers(options)
	
	if options.assemble:
		singleKAssemblies(options,kmin,kmax,kstep)

	if options.kmerge:
		mergeAssemblies(options,kmin,kmax,kstep)

	if options.clean:
		clean(options,kmin,kmax,kstep)

if __name__ == "__main__":
	main()
