Because of the use of fixed-length arrays, a number of variables have to be set before compile both Velvet and Oases.

Moreover the compilation of Oases need Velvet to be already compiled.

Please edit the Makefile with the appropriate parameters to compile both Velvet and Oases. Then type make install to download compile and install Velvet and Oases in the current project.
